app.directive("ngLivepreivew",["$timeout",function($timeout){
	return  {
		scope : {
			cloneId : "="
		},
		link : function(scope, $elem){
			console.log(scope.cloneId);
			console.log($elem);

			var target = {};

	

			window.addEventListener("load",function(){
				console.log("load");

				$timeout(function(){

				scope.cloneId.forEach(function(item){

					var child = document.createElement("div");
					var img		= document.createElement("img");
					var label = document.createElement("button");

					label.setAttribute("ng-scrollto",item.selector);

					label.innerHTML = item.name;

					target.$elem = $(item.selector);
					target.width = target.$elem.width();
					target.height = target.$elem.height();

					child.appendChild(img);
					child.appendChild(label);
					$elem[0].appendChild(child);

					getScreenshotOfElement(target.$elem.get(0), 0, 0, target.width, target.height, function(data) {
					// in the data variable there is the base64 image
					// exmaple for displaying the image in an <img>
						$(img).attr("src", "data:image/png;base64,"+data);
					
					});

				});

				var active = $elem[0].children[0];
				$(active).addClass("active");

				$($elem[0].children).hover(
					function(){
						//console.log(active);
						$(active).removeClass("active");
						active = this;
						$(active).addClass("active");
					},
					function(){
						//console.log(this);
						//$(active).removeClass("active");
					}
				);



				$elem.find("button").on("click",function(){
					////console.log(scope.ngScrollto);
					targetElement = $($(this).attr("ng-scrollto"));
					targetElement.velocity('scroll', {
						duration: 500,
						offset: -65,
						easing: 'ease-in-out'
					});
				});

				},0);


			});

						
			
			function getScreenshotOfElement(element, posX, posY, width, height, callback) {
				html2canvas(element, {
					onrendered: function (canvas) {
						var context = canvas.getContext('2d');
						var imageData = context.getImageData(posX, posY, width, height).data;
						var outputCanvas = document.createElement('canvas');
						var outputContext = outputCanvas.getContext('2d');
						outputCanvas.width = width;
						outputCanvas.height = height;

						var idata = outputContext.createImageData(width, height);
						idata.data.set(imageData);
						outputContext.putImageData(idata, 0, 0);
						callback(outputCanvas.toDataURL().replace("data:image/png;base64,", ""));
					},
					width: width,
					height: height,
					useCORS: true,
					taintTest: true,
					allowTaint: true
				});
			}

			//
		}
	};
}]);

//DIRECTIVE TO MOVE BACKGROUND ON USER HOVER
app.directive("ngHoverFloat",[function(){
	return {
		link : function(scope, $elem){
//			console.log($elem);
			
			var lFollowX = 0,
				lFollowY = 0,
				x = 0,
				y = 0,
				friction = 1 / 30;

			function moveBackground() {
			x += (lFollowX - x) * friction;
			y += (lFollowY - y) * friction;
			
			translate = 'translate(' + x + 'px, ' + y + 'px) scale(1.1)';

			$elem.css({
				'-webit-transform': translate,
				'-moz-transform': translate,
				'transform': translate
			});

			window.requestAnimationFrame(moveBackground);
			}

			$(window).on('mousemove click', function(e) {

			var lMouseX = Math.max(-100, Math.min(100, $(window).width() / 2 - e.clientX));
			var lMouseY = Math.max(-100, Math.min(100, $(window).height() / 2 - e.clientY));
			lFollowX = (20 * lMouseX) / 100; // 100 : 12 = lMouxeX : lFollow
			lFollowY = (10 * lMouseY) / 100;

			});

			moveBackground();
		}
	}
}]);

app.directive("ngNotification",[function(){
	return {
		templateUrl : "views/notification.html",
		scope : {
			model : "="
		},
	}
}]);

//Navigation popup
app.directive("ngPageNav",[function(){
	return {
		templateUrl : "views/page-nav-menu.html",
		scope : {
			data : "="
		},
		link : function(scope, $elem){
			// scope.$watch("data",function(newValue){
			// 	console.log(newValue);
			// });


			window.addEventListener("keydown",function(event){
				//console.log(event);
				if(event.keyCode = 27){
					scope.data.hide();
					scope.$apply();
				}
			});

		//	console.log("ngpagenav");
		}
	};
}]);

app.directive("ngDashnav",[function(){
	return {
		templateUrl : "views/dashboard-nav.html",
		scope : {
			activename : "@",
			items : "="
		}
	}
}]);

app.directive("ngLogoNavinternal",[function(){
	return {
		templateUrl : "views/logo-nav-internal.html",
		scope : {
			activename : "@"
		}
	}
}]);

app.directive("ngInnernav",[function(){
	return {
		scope : {
			activename : "@",
			items : "="
		},
		templateUrl : "views/inner-nav.html",
		link : function(scope){
			console.log(scope);
		}
	};
}]);

app.directive("ngFooter",[function(){
	return {
		templateUrl : "views/common-footer.html"
	};
}]);

app.directive("ngInitSkrollr",[function(){
	return {
		link : function(scope){
			scope.skrollr = skrollr.init();
		}
	};
}]);


//DECLARGIN ALL DIRECTIVES IN THIS SECTION
app.directive("ngLoading",function(){
	return {
		template : "<table class='pop-up' ng-fadeshow='ngLoading.state' ng-cloak><tr><td class='text-center whiteFont'><div style='max-width:400px; margin:0px auto'><div class='text-right'><a href='javascript:void(0)' ng-click='ngLoading.hide()'><small>X</small></a></div><br>NEXUS<br><p ng-bind='ngLoading.msg'></p></div></td></tr></table>",
		scope : {
			ngLoading : '='
		},
		link : function(scope, $elem){
			////console.log($elem);
		}
	}
});

app.directive("ngSlider",[function(){
	return {
		scope : {
			ngSlider : "="
		},
		link : function(scope, $elem){

			//console.log(scope.ngSlider);

			scope.$watch("ngSlider",function(newValue){
				if(newValue.list.length != undefined && newValue.list.length != 0)
					initSlider(newValue);
			});


			var $ul = $(document.createElement("ul")),
				$li;
			
			function initSlider(sliderData){

				sliderData.list.forEach(function(key){

					$li = $(document.createElement("li"));

					$li.append("<img src='"+key.imgSrc+"'>");
					$ul.append($li);
				});

				$elem.append($ul);

				scope.ngSlider.options.onSlideBefore = function(currentSlide, totalSlides, currentSlideHtmlObject){
					scope.ngSlider.currentIndex = slider.getCurrentSlide();
					scope.$apply();
				}

				var slider = $ul.bxSlider(scope.ngSlider.options),
					goNext,
					userPauseDebounce;

				scope.ngSlider.currentIndex = slider.getCurrentSlide();

			}

		}
	};
}]);


app.directive("ngBxslider",function(){
	return {
		scope : {
			ngBxslider : "="
		},
		link : function(scope, $elem){
			
			//console.log(scope.ngBxslider);

			var $ul = $(document.createElement("ul")),
				$li;

			scope.ngBxslider.items.forEach(function(key){

				$li = $(document.createElement("li"));


				$li.append("<img src='"+key.imgSrc+"'>");

				
				if(key.textContent){
					$li.append("<div class='text-content'><div class='container-nexus self-clear relative-100Height'><div class='textbox greyBg greyFont absolute-100Hegiht'><table><tr><td class='padding-15 darkGreyFont'>"+key.textContent+"</td></tr></table></div></div></div>");

				}
				$ul.append($li);
			});


			$elem.append($ul);


			scope.ngBxslider.options.onSliderLoad = function(){
				//console.log("Slider load");

				var _self = this;
				//console.log(this);
				if(scope.ngBxslider.options.timeIndicator){

					//bx-controls
					var timeIndicator = document.createElement("div");
					var handle = document.createElement("div");
					
					timeIndicator.appendChild(handle);
					timeIndicator.setAttribute("id","timeoutIndicator");
					handle.setAttribute("id","handle");
					//angular.element(".bx-controls")
					//console.log($elem.find(".bx-controls"));
					$elem.find(".bx-controls")[0].appendChild(timeIndicator);


					goNext = function(){
						$(handle).velocity({"width":["100%",'0%']},5000, function(){
							//console.log("do it");
							//slider.goToNextSlide();
							
						});
					}

					goNext();

					$elem.find(".bx-pager-item").on("click",function(){
						clearTimeout(userPauseDebounce);
						userPauseDebounce = setTimeout(function(){
							//slider.goToNextSlide();
							slider.startAuto();

							//goNext();
						},5000);
					});

					

				}
			}

			//Attacheing next function so that we can change teh slide form other directive.
			scope.ngBxslider.next = function(){
				slider.goToNextSlide();
			}

			scope.ngBxslider.options.onSlideNext = function(){
				//console.log("onSlideNext");
				if(scope.ngBxslider.options.timeIndicator)
					goNext();
			}


			var slider = $ul.bxSlider(scope.ngBxslider.options),
				goNext,
				userPauseDebounce;
		}
	}
});

//WHILE SCROLLING THROGH THE PAGE, THIS DIRECTIVE WILL FIND THE ACTIVE SECTION AND GET THE VALUE
app.directive("ngViewIndicator",[function(){
	return {
		scope : {
			ngViewIndicator : "="
		},
		link : function(scope, $elem){
			
			HTMLCollection.prototype.forEach = Array.prototype.forEach;
			
//			console.log(scope);
//			console.log($elem);



			var sections = [],
				names = scope.ngViewIndicator,
				debounce;

			document.addEventListener("scroll",function(e){
				clearTimeout(debounce);
				debounce = setTimeout(function(){
					//console.log(window.pageYOffset);
					setActive();
				}, 50);
			});


			function setActive(){

//				console.log("set active");
				sections.forEach(function(key){
					
					//console.log(key);
					
					
					var screenMiddle = window.pageYOffset + window.innerHeight / 2;


				//	console.log(screenMiddle);

					if(screenMiddle >= key.offsetTop && screenMiddle <= (key.offsetTop + key.offsetHeight)){
						
						scope.ngViewIndicator.activeName = key.name;
					//	console.log(key);
						//console.log(scope.ngViewIndicator);
						$elem.html(key.name);
						//scope.$apply();
					}
				});
			}

			function recordOffsets(){
			
				var classes = document.getElementsByClassName(names.className);

				classes.forEach(function(key, index){
					
					//console.log(key);

					sections.push({

						name : key.getAttribute("data-section"),
						offsetTop : key.getBoundingClientRect().top,
						offsetHeight : key.getBoundingClientRect().height

					});
				});

				if(sections.length == 1){
					scope.ngViewIndicator.activeName = sections[0].name;
					$elem.html(sections[0].name);
				}
			}

			recordOffsets();
			setActive();

			window.addEventListener("load",function(){
				recordOffsets();
			});
//			console.log(sections);
			
			


		}
	};
}]);


app.directive("ngTopslide",function(){
	return {
		scope : {
			ngTopslide : "="
		},
		link : function(scope, $elem){
			console.log(scope.ngTopslide.options);
			
			scope.ngTopslide.options.onSlideAfter = function(currentSlide, totalSlides, currentSlideHtmlObject){
				//console.log(currentSlide);
				//$elem.parent().velocity({height : currentSlide[0].clientHeight + "px"});
				
				
				// if(window.pageYOffset > 300)
				// currentSlide.velocity('scroll', {
    //         		duration: 1000,
    //         		offset: -40,
    //         		easing: 'ease-in-out'
    //     		});
			}


			var slider = $elem.bxSlider(scope.ngTopslide.options);
			//console.log(slider);
			// console.log(slider.getCurrentSlideElement());

			// $elem.parent().velocity({height : slider.getCurrentSlideElement()[0].clientHeight + "px"});

			scope.ngTopslide.goto = function(index){
				//console.log($elem);
				slider.goToSlide(index);

			}

			var debounce;

			// window.addEventListener("resize",function(){
			// 	clearTimeout(debounce);
			// 	debounce = setTimeout(function(){
			// 		$elem.parent().velocity({height : slider.getCurrentSlideElement()[0].clientHeight + "px"});
			// 	},500);
			// });
		}
	}
})

app.directive("ngNavscroll",function(){
	return {
		templateUrl : "views/top-logos.html",
		link : function(scope, $elem){

			var fixed = false;

			window.addEventListener("scroll",function(e){

				if(window.pageYOffset > 0 && fixed == false){
					$elem.addClass("fixed");
					fixed = true;
					////console.log("Fire");
				} else if(window.pageYOffset <= 0 && fixed == true){
					$elem.removeClass("fixed");
					fixed = false;
					////console.log("Fire");
				}

			});



		}
	}
});

//Bind data to the element. Alternative for ng-bind-html
app.directive("ngCustombind",function(){
	return {
		scope : {
			ngCustombind : "="
		},
		link : function(scope, $elem){			
			scope.$watch('ngCustombind', function(newValue, oldValue) {
				$elem.html(scope.ngCustombind);
			});
		}
	}
});

app.directive("ngRoundslider",function(){
	return {
		scope : {
			ngRoundslider : "="
		},
		link : function(scope, $elem){			
			//scope.$watch('ngRoundslider', function(newValue, oldValue) {
			
			

			$elem.roundSlider({
			    radius: 200,
			    width: 45,
			    circleShape: "quarter-top-right",
			    sliderType: "min-range",
			    showTooltip: false,
			    value: 100,
			    change: function (e) {
			        console.log(e.value);
			        if(e.value == 0){
			        	scope.ngRoundslider.next();
			        	roundslider.setValue(100);
			        }
			    }
			});

			var roundslider = $elem.data("roundSlider");

			//});
		}
	}
});

app.directive("ngCountnumber",function(){
	return {
		scope : {
			ngCountnumber : "="
		},
		link : function(scope, $elem){			
			scope.$watch('ngCountnumber', function(newValue, oldValue) {
				//$elem.html(scope.ngCountnumber);
				$elem.animate({
			        Counter: newValue
			    }, {
			        duration: 4000,
			        easing: 'swing',
			        step: function (now) {
			            $(this).text(Math.ceil(now));
			        }
			    });
			});
		}
	}
});

app.directive("ngNoPropa",function() {
	return {
		restrict : "A",
		link : function (scope, $elem, $attr) {
			$elem.on("click",function(e){
				e.stopPropagation();
			});
		}
	}
});

app.directive("ngFadeshow",function(){
	return {
		scope : {
			ngFadeshow : "="
		},
		link : function(scope, $elem){			
			scope.$watch('ngFadeshow', function(newValue, oldValue) {

				if(newValue){
					$elem.addClass("active");
					$elem.velocity({opacity : [1,0]},300);
				} else {

					$elem.velocity({opacity : [0,1]},300,function(){
						$elem.removeClass("active");
					});
				}
				////console.log(newValue);
				
			});
		}
	}
});

app.directive("ngSlideleftshow",function(){
	return {
		scope : {
			ngSlideleftshow : "="
		},
		link : function(scope, $elem){			
			
			var speed = 200;

			scope.$watch('ngSlideleftshow', function(newValue, oldValue) {

				
				var width = $elem.width();

				if(newValue){
					$elem.addClass("active");
					$elem.velocity({left : "0px"}, {duration : speed});
				} else {

					$elem.velocity({left : "-"+ width + "px"},speed,function(){
						$elem.removeClass("active");
					});
				}
				////console.log(newValue);
				
			});
		}
	}
});


app.directive("ngScrollto",function(){
	return {
		scope : {
			ngScrollto : "="
		},
		link : function(scope, $elem){			
			
			var targetElement = $("#"+scope.ngScrollto);


			$elem.on("click",function(){
				////console.log(scope.ngScrollto);
				targetElement.velocity('scroll', {
		            duration: 500,
		            offset: -40,
		            easing: 'ease-in-out'
		        });
			});

		}
	}
});

app.directive("ngNav",function(){
	return {
		scope : {
			ngNav : "="
		},
		link : function(scope, $elem){	

			//console.log(scope.ngNav);
			var targetElement = null;
			var targetSection = null;
			var activeSection = null;

			//Add unique ID attribute to all div with class with section.
			angular.element(".section").each(function(key,value){
				value.setAttribute("id",scope.ngNav.list[key].hrefId);
			});

			scope.ngNav.list.forEach(function(key,index){
				key.anchor = $(document.createElement("li")).addClass("list-group-item cursor-pointer");
				key.anchor.append("<img src="+key.imgSrc+"><label>"+key.label+"</label>");
				$elem[0].appendChild(key.anchor[0]);


				key.anchor.on("click",function(){
					////console.log(scope.ngScrollto);
					if(targetElement != null)
						targetElement.removeClass("active");
					targetElement = $("#"+key.hrefId);
					targetElement.addClass("active");

					//console.log(targetElement);

					targetElement.velocity('scroll', {
			            duration: 500,
			            offset: -40,
			            easing: 'ease-in-out'
			        });
				});


			});

			console.log(scope.ngNav);
			console.log($elem[0]);

			var activeNav = null;


			function checkPagesection(){
				//console.log(window.pageYOffset);

				scope.ngNav.list.forEach(function(key, index){

					

					targetSection = document.getElementById(key.hrefId);
					//console.log(targetElement);
					//console.log(targetElement.offsetTop);
					//console.log(targetElement.clientHeight);
					var screenMiddle = window.pageYOffset + window.innerHeight / 2;

					if(screenMiddle >= targetSection.offsetTop && screenMiddle <= (targetSection.offsetTop +targetSection.offsetHeight)){
						//console.log(targetElement);



						if(activeNav != null){
							activeNav.removeClass("active");
							
						}

						activeNav = $($elem[0].children[index]).addClass("active");
						if(activeSection)
							activeSection.removeClass("active");

						activeSection = $(targetSection);
						activeSection.addClass("active");

					}
				});
			}

			var debounce = null
			
			window.addEventListener("scroll",function(){
				clearTimeout(debounce);
				debounce = setTimeout(function(){
					checkPagesection();
				},20);
			});


			window.addEventListener("load",function(){
				checkPagesection();
			});

		}
	}
});



app.directive("ngFullscreen",function(){
	return {
		scope : {
			ngFullscreen : "="
		},
		link : function(scope, $elem){			
			
			scope.$watch("ngFullscreen",function(newVal, oldVal){

				if(newVal === true){

					$elem.css({"z-index":10});
					$elem.velocity({ width : ["100%","50%"], height : ["100%","50%"] })

				}

			});
		}
	}
});

//Work with Velocity JS
//User true or false in the directive to control it.
app.directive("ngExpanded",function(){
	return {
		scope : {
			ngExpanded : "=",
			speed : "="
		},
		link : function (scope, $elem, $attr){

			var firstTime = true,
				expansionSpeed = scope.speed || 700;

			// scope.$watch("scope",function(){
				// console.log("Scope change");
			// });

			scope.$watch("ngExpanded",function(){
				//console.log(scope.ngExpanded);
				
				if(firstTime && !scope.ngExpanded){
					$elem.css({"height":"0px"});
				}

				else if(!firstTime && scope.ngExpanded){

					setTimeout(function(){
						var height = $elem[0].scrollHeight;
						$elem.velocity({"height":height+"px"}, expansionSpeed, function(){
							$elem.css({"height":'auto'});
							
							var top = $elem[0].offsetTop - (document.body.clientHeight / 2);
							// $("html").velocity("scroll", { offset: top+"px", mobileHA: true, duration: 1000});

						});

						
					},100);		
				}

				else if(!firstTime && !scope.ngExpanded){
					$elem.velocity({"height":"0px"}, expansionSpeed);
				}


				firstTime = false;
			});
		}
	}
});

//COMPLEX CHARTS
app.directive("ngAreachart",function(){
	return {

		scope : {
			ngAreachart : "="
		},
		link : function(scope, $elem){

			////console.log(scope.ngAreachart);

			$elem.attr({"id":scope.ngAreachart.containerId});

			Highcharts.chart(scope.ngAreachart.containerId, {
			    chart: {
			        type: 'areaspline',
			        backgroundColor: '#0397ab',
			        style: {
            			fontFamily: 'monospace',
            			color: "#fff"
        			}
			    },

			    title: {
			        text: ''
			    },
			    legend: {
			    	enabled : false,
			        layout: 'vertical',
			        align: 'left',
			        verticalAlign: 'top',
			        x: 150,
			        y: 100,
			        floating: true,
			        borderWidth: 1,
			        backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
			    },
			    xAxis: {
			        categories: scope.ngAreachart.categories,
			        plotBands: [{ // visualize the weekend
			            from: 4.5,
			            to: 6.5,
			            color: 'rgba(68, 170, 213, .2)'
			        }],
			        labels: {
			            style: {
			                color: '#fff'
			            }
			        }
			    },
			    yAxis: {
			        title: {
			            text: '',
			            style: {
	            			fontFamily: 'monospace',
	            			color: "#fff"
	        			}
			        },
			        labels: {
			            style: {
			                color: '#fff'
			            }
			        }

			    },
			    tooltip: {
			        shared: true,
			        valueSuffix: ' units'
			    },
			    credits: {
			        enabled: false
			    },
			    plotOptions: {
			        areaspline: {
			            fillOpacity: 0.5
			        }
			    },
			    series: scope.ngAreachart.series
			});

		
			var labelContainer = document.createElement("ul"),
				li;

			scope.ngAreachart.series.forEach(function(key){
				li = document.createElement("li");
				li.innerHTML = key.name;
				angular.element(li).attr({"id":key.name});
				labelContainer.appendChild(li);
			});

			labelContainer.setAttribute("class","labels");
			$elem[0].appendChild(labelContainer);

		}

	}
});

app.directive("ngMultipleslider",function(){
	return {
		scope : {
			ngMultipleslider : "="
		},
		link : function(scope, $elem){


			var container = document.createElement("div"),
				itemMask = document.createElement("div"),
				itemsContainer = document.createElement("div"),
				buttonLeft = document.createElement("button"),
				buttonRight = document.createElement("button"),
				item;


			var dim = {}


			dim.width = $elem[0].offsetWidth;
			dim.itemWidth = (dim.width / 3);
			dim.itemWidthPerc = 100 / scope.ngMultipleslider.length;
			dim.offsetLeft = 0;

			window.addEventListener("resize",function(){
				setDimensions();
			});


			function setDimensions(){
				dim.width = $elem[0].offsetWidth;

				if(document.documentElement.clientWidth < 768){
					dim.itemWidth = (dim.width / 2);
				}
					
				else{
					dim.itemWidth = (dim.width / 3);
				}
					
				$(itemsContainer).css({"width": (dim.itemWidth * scope.ngMultipleslider.length) + "px"});
			}

			setDimensions();


			scope.ngMultipleslider.forEach(function(key){
				$item = $(document.createElement("div"));
				$item.addClass("item");

				if(key.class)
					$item.addClass(key.class);

				$item.append(key.contents);

				$item.css({"width":dim.itemWidthPerc + "%"});

				var anchor = document.createElement("a");
				anchor.href = key.href;
				anchor.appendChild($item[0]);

				itemsContainer.appendChild(anchor);
			});


			$(container).addClass("multipleslider");
			$(itemMask).addClass("mask");
			$(itemsContainer).addClass("itemscontainer");

			buttonRight.innerHTML = "&rsaquo;";
			buttonLeft.innerHTML =  "&lsaquo;";

			$(buttonLeft).addClass("control-btn btn-left");
			$(buttonRight).addClass("control-btn btn-right");

			container.appendChild(itemMask);
			itemMask.appendChild(itemsContainer);
			container.appendChild(buttonLeft);
			container.appendChild(buttonRight);
			$elem[0].appendChild(container);

			console.log(dim);

			var activeIndex = 1;
			$(itemsContainer.children[activeIndex]).addClass("active");

			buttonRight.addEventListener("click",function(){

				buttonLeft.disabled = false;

				if(activeIndex >= scope.ngMultipleslider.length - 3){
					buttonRight.disabled = true;
					//return;
				}
					

				$(itemsContainer.children[activeIndex]).removeClass("active");
				activeIndex++;
				$(itemsContainer.children[activeIndex]).addClass("active");


				dim.offsetLeft = itemsContainer.offsetLeft - dim.itemWidth;
				$(itemsContainer).velocity({left : dim.offsetLeft + "px"});
			});

			buttonLeft.disabled = true;
			if(activeIndex >= scope.ngMultipleslider.length - 3){
				buttonRight.disabled = true;
				//return;
			}

			buttonLeft.addEventListener("click",function(){

				buttonRight.disabled = false;

				if(activeIndex <= 2){
					buttonLeft.disabled = true;
					//return;
				}
					

				$(itemsContainer.children[activeIndex]).removeClass("active");
				activeIndex--;
				$(itemsContainer.children[activeIndex]).addClass("active");

				dim.offsetLeft = (itemsContainer.offsetLeft + dim.itemWidth > 0)? 0 : (itemsContainer.offsetLeft + dim.itemWidth);
				$(itemsContainer).velocity({left : dim.offsetLeft + "px"});
			});

		}
	}
});