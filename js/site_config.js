var config = {}


config.routes = {
    about : [
        {
            label : "Program Introduction",
            href : "about-the-program.html",
        },
        {
            label : "How To Earn Points",
            href : "about-how-to-earn.html",
        },
        {
            label : "How To Redeem Points",
            href : "about-how-to-redeem.html",
        },
        {
            label : "How To Participate",
            href : "about-how-to-participate.html",
        },
    ],

    dashboards : [
        {
            label : "Leaderboard",
            href : "dashboards-leaderboard.html"
        },
        {
            label : "Target vs Acheivements",
            href : "dashboards-target-vs-achievement.html"
        },
        {
            label : "Sales vs Earning",
            href : "dashboard-sales-vs-earning.html"
        },

    ],

    promotions : [
        {
            label : "Twice the Mix",
            href : "promotion-twice-the-mix.html"
        },
        {
            label : "Bully’s Eye & Beyond",
            href : "promotion-bullys-eye.html"
        },
        {
            label : "Trip to Bali",
            href : "promotion-trip-to-bali.html"
        },
    ],

    articles : [
        {
            date : "01/01/2017",
            imgSrc : "img/articles/article1.jpg",
            label : "Scientifically-backed",
            href : "article-scientifically-backed.html",
            title : "<h4 class='section-titles pinkRedFont'>Scientifically-backed Tips to Help You Sell More.</h4><p>As a small business owner with a retail store, you probably spend a lot of resources getting people to walk through your doors. But, what do you do once they're in?</p>"
        },
        {
            date : "01/07/2017",
            imgSrc : "img/articles/article2.jpg",
            label : "Dealer 101",
            href : "article-dealer101.html",
            title : "<h4 class='section-titles pinkRedFont'>Dealer 101: How to Read Your Customers</h4><p>Are you a retailer (or retail sales associate) who’s struggling with how to approach shoppers? Worried that you lack the magic touch, or that you’ll come off as an annoying salesperson? Would you rather be awkwardly staring at your store’s point of sale software screen than actually talking to the customer in front of you?</p>"
        },
        {
            date : "01/07/2017",
            imgSrc : "img/articles/article3.jpg",
            label : "How to handle",
            href : "article-how-to-handle.html",
            title : "<h4 class='section-titles pinkRedFont'>How to Handle a Slump in Retail Sales</h4><p>If you are a retailer, it's inevitable — sales will slump. Whether it's due to forces beyond your control like the city tearing up the street in from of your store or seasonal sales dip or a decline in foot traffic, all retailers will experience a slump in sales at some point.</p>"
        },
    ],

    reports : [
        {
            label : "Sales Promotions",
            href : "report-sales-promotion.html",
        },
    ],

};

config.pageNav = {
    items : [
        {
            label : "Home",
            color : "#f8768c",
            href : "index.html#home"
        },
        {
            label : "My Account",
            color : "#f54c45",
            sub_items : [
                {
                    label : "Edit Profile",
                    href : "editprofile.html"
                },
                {
                    label : "Earn History",
                    href : "earnhistory.html"
                },
            ],
        },
        {
            label : "Dashboard",
            color : "#fead42",
            href : "dashboards.html",
        },
        {
            label : "Promotions",
            color : "#8bca35",
            sub_items : config.routes.promotions
        },
        {
            label : "Rewards",
            color : "#21a9dd",
            href : "index.html#rewards"
        },
        {
            label : "About",
            color : "#A125ED",
            sub_items : config.routes.about,
        },
        {
            label : "Articles",
            color : "#e8058a",
            href : "articles-list.html"
        },
        {
            label : "Reports",
            color : "#6a84dd",
            sub_items : config.routes.reports,
        },
        {
            label : "Home",
            color : "#f8768c",
            href : "index.html#home"
        },
        {
            label : "My Account",
            color : "#f54c45",
            sub_items : [
                {
                    label : "Edit Profile",
                    href : "editprofile.html"
                },
                {
                    label : "Earn History",
                    href : "earnhistory.html"
                },
            ],
        },
        {
            label : "Dashboard",
            color : "#fead42",
            href : "dashboards.html",
        },
        {
            label : "Promotions",
            color : "#8bca35",
            sub_items : config.routes.promotions
        },
        {
            label : "Rewards",
            color : "#21a9dd",
            href : "index.html#rewards"
        },
        {
            label : "About",
            color : "#A125ED",
            sub_items : config.routes.about,
        },
        {
            label : "Articles",
            color : "#e8058a",
            href : "articles-list.html"
        },
        {
            label : "Reports",
            color : "#6a84dd",
            sub_items : config.routes.reports,
        },
        {
            label : "Home",
            color : "#f8768c",
            href : "index.html#home"
        },
        {
            label : "My Account",
            color : "#f54c45",
            sub_items : [
                {
                    label : "Edit Profile",
                    href : "editprofile.html"
                },
                {
                    label : "Earn History",
                    href : "earnhistory.html"
                },
            ],
        },
        {
            label : "Dashboard",
            color : "#fead42",
            href : "dashboards.html",
        },
        {
            label : "Promotions",
            color : "#8bca35",
            sub_items : config.routes.promotions
        },
        {
            label : "Rewards",
            color : "#21a9dd",
            href : "index.html#rewards"
        },
        {
            label : "About",
            color : "#A125ED",
            sub_items : config.routes.about,
        },
        {
            label : "Articles",
            color : "#e8058a",
            href : "articles-list.html"
        },
        {
            label : "Reports",
            color : "#6a84dd",
            sub_items : config.routes.reports,
        },
        {
            label : "Home",
            color : "#f8768c",
            href : "index.html#home"
        },
        {
            label : "My Account",
            color : "#f54c45",
            sub_items : [
                {
                    label : "Edit Profile",
                    href : "editprofile.html"
                },
                {
                    label : "Earn History",
                    href : "earnhistory.html"
                },
            ],
        },
        {
            label : "Dashboard",
            color : "#fead42",
            href : "dashboards.html",
        },
        {
            label : "Promotions",
            color : "#8bca35",
            sub_items : config.routes.promotions
        },
        {
            label : "Rewards",
            color : "#21a9dd",
            href : "index.html#rewards"
        },
        {
            label : "About",
            color : "#A125ED",
            sub_items : config.routes.about,
        },
        {
            label : "Articles",
            color : "#e8058a",
            href : "articles-list.html"
        },
        {
            label : "Reports",
            color : "#6a84dd",
            sub_items : config.routes.reports,
        },
        {
            label : "Home",
            color : "#f8768c",
            href : "index.html#home"
        },
        {
            label : "My Account",
            color : "#f54c45",
            sub_items : [
                {
                    label : "Edit Profile",
                    href : "editprofile.html"
                },
                {
                    label : "Earn History",
                    href : "earnhistory.html"
                },
            ],
        },
        {
            label : "Dashboard",
            color : "#fead42",
            href : "dashboards.html",
        },
        {
            label : "Promotions",
            color : "#8bca35",
            sub_items : config.routes.promotions
        },
        {
            label : "Rewards",
            color : "#21a9dd",
            href : "index.html#rewards"
        },
        {
            label : "About",
            color : "#A125ED",
            sub_items : config.routes.about,
        },
        {
            label : "Articles",
            color : "#e8058a",
            href : "articles-list.html"
        },
        {
            label : "Reports",
            color : "#6a84dd",
            sub_items : config.routes.reports,
        },
        {
            label : "Home",
            color : "#f8768c",
            href : "index.html#home"
        },
        {
            label : "My Account",
            color : "#f54c45",
            sub_items : [
                {
                    label : "Edit Profile",
                    href : "editprofile.html"
                },
                {
                    label : "Earn History",
                    href : "earnhistory.html"
                },
            ],
        },
        {
            label : "Dashboard",
            color : "#fead42",
            href : "dashboards.html",
        },
        {
            label : "Promotions",
            color : "#8bca35",
            sub_items : config.routes.promotions
        },
        {
            label : "Rewards",
            color : "#21a9dd",
            href : "index.html#rewards"
        },
        {
            label : "About",
            color : "#A125ED",
            sub_items : config.routes.about,
        },
        {
            label : "Articles",
            color : "#e8058a",
            href : "articles-list.html"
        },
        {
            label : "Reports",
            color : "#6a84dd",
            sub_items : config.routes.reports,
        },
        {
            label : "Home",
            color : "#f8768c",
            href : "index.html#home"
        },
        {
            label : "My Account",
            color : "#f54c45",
            sub_items : [
                {
                    label : "Edit Profile",
                    href : "editprofile.html"
                },
                {
                    label : "Earn History",
                    href : "earnhistory.html"
                },
            ],
        },
        {
            label : "Dashboard",
            color : "#fead42",
            href : "dashboards.html",
        },
        {
            label : "Promotions",
            color : "#8bca35",
            sub_items : config.routes.promotions
        },
        {
            label : "Rewards",
            color : "#21a9dd",
            href : "index.html#rewards"
        },
        {
            label : "About",
            color : "#A125ED",
            sub_items : config.routes.about,
        },
        {
            label : "Articles",
            color : "#e8058a",
            href : "articles-list.html"
        },
        {
            label : "Reports",
            color : "#6a84dd",
            sub_items : config.routes.reports,
        },

    ]
};


