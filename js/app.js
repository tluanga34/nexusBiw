var app = angular.module("app",[]);


//WE WILL KEEP ALL THE COMMONLY USED CONSTRUCTORS INSIDE THIS NAME SPACE, SO WE CAN AVOID GLOBAL NAMESPACE
app.Consts = {};


//DEFINING CONSTRUCTOR AND PROTOTYPE FUNCTIONS FOR ALL TYPES OF TOGGLING MODELS
app.Consts.Toggle = function(defaultState){
	this.state 	= defaultState || false;
	this.msg	= '';
}

app.Consts.Toggle.prototype.show = function(msg){
	if(!this.state)
		this.state = true;

	if(msg != undefined)
		this.msg = msg;
};

app.Consts.Toggle.prototype.hide = function(){
	if(this.state)
		this.state = false;

	if(this.msg != '')
		this.msg = '';
};
app.Consts.Toggle.prototype.toggle = function(){
	this.state = !this.state;
}


//DEFINING CONSTRUCTOR FOR ALL KINDS OF OBJECT WHICH HAS VALUE, LIST, AND OPTIONS
app.Consts.Data = function(){
	this.value = '';
	this.list = [];
	this.options = null;
}

app.Consts.Data.prototype.setList = function(data){
	this.list = data;
}

app.Consts.Data.prototype.setValue = function(data){
	this.value = data;
}

app.Consts.Data.prototype.setOptions = function(data){
	this.options = data;
}