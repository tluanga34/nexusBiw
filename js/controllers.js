app.controller("TopController",["$scope","dataModel",function(s,dataModel){

	//CONSTRUCT OBJECTS		
	s.menu 				= new app.Consts.Toggle(false);
	s.mainPageLoading	= new app.Consts.Toggle(false);
	s.bodyScroll		= new app.Consts.Toggle(true);

	s.nav 				= new dataModel.Construct(false);
	s.notification		= new dataModel.Construct(false);

	s.bannerSlide 		= dynamicData.topBannerSlide;
	s.brandProperties	= dynamicData.brandProperties;
	s.userInfo			= dynamicData.userInfo;

	s.sectionFinder = {className : 'section', uniqueAttr : 'data-section'};

	
	//INITIATE THE PROGRAM
	//if(dynamicData.pageNav)
	s.nav.setList(config.pageNav.items);

	//Show Loading page while page load images
	// s.mainPageLoading.show("Loading Page. Please wait");

	//Remove the loading page when window loaded.
	window.addEventListener("load",function(){
		//console.log(s);
		//s.mainPageLoading.hide();
		s.$apply();
	});

	//if(dynamicData.userInfo.message){
		//s.notification.showMsg(dynamicData.userInfo.message);
		
	//}

	if(dynamicData.page)
		s.pagePreview = dynamicData.page.preview;

	


}]);

app.controller("loginController",["$scope",function(s){

	//console.log("LoginController");

	function Form(){

		var self = this;

		self.username = new app.Consts.Data();
		self.password = new app.Consts.Data();

		//self.password.type = 'password';

		self.seePassword = new app.Consts.Toggle(false);
	}

	s.form = new Form();

}]);


app.controller("EarnHistory",["$scope",function(s){

	s.earnHistory = {}
	s.earnHistory.data = dynamicData.earnHistory

}]);
app.controller("Dashboards",["$scope",function(s){

	//console.log("Dashboards");

	s.leaderBoard = new TableData(dynamicData.leaderBoardData || {});
	s.leaderBoard.rotated = true ;

	s.targetvsAch = dynamicData.targetVsAchievements || {};
	s.targetvsAch.rotated = true;

	s.salesVsEarning = dynamicData.salesVsEarning || {};
	s.salesVsEarning.rotated = true;

	s.routes = config.routes.dashboards || {};

	function TableData(data){

		////console.log(data);
		this.data = data;

	}


}]);


app.controller("Rewardsgallery",["$scope",function(s){

	s.rewardsGalleryItems = dynamicData.rewardsgallery;


}]);

app.controller("Profilemenu",["$scope",function(s){

	s.profilemenu = dynamicData.profileMenu;


}]);


app.controller("Articles",["$scope",function(s){

	s.articles = config.routes.articles;


}]);


app.controller("UpcommingPromotions",["$scope","dataModel",function(s,dataModel){

	s.upcomingPromotions = dynamicData.upcomingPromotions;
	s.promotions = new dataModel.Construct();
	s.promotions.setList(dynamicData.upcomingPromotions.items);
	s.promotions.options = dynamicData.upcomingPromotions.options;

	s.routes = config.routes.promotions;

}]);

app.controller("AboutPrograms",["$scope","$timeout",function(s,$timeout){

	s.aboutProgram = new app.Consts.Data();

	if(dynamicData.aboutProgram)
		s.aboutProgram.setList(dynamicData.aboutProgram.programs);

	s.routes = config.routes.about;

	//console.log("AboutPrograms");
	//console.log(s.aboutProgram);


	s.aboutProgram.list.forEach(function(key){
		key.pop = new app.Consts.Toggle(false);
	});


	s.activeProgram = {};

	s.setActiveProgram = function(x){

		if(s.activeProgram != x){
			
			if(s.activeProgram.pop)
				s.activeProgram.pop.hide();
			setDownZindex(s.activeProgram);
			
		}
			
		s.activeProgram = x;
		s.activeProgram.zIndex = 10;
		s.activeProgram.pop.show();
		//s.$parent.bodyScroll.hide();
	}

	function setDownZindex(x){
		//s.$parent.bodyScroll.show();
		$timeout(function(){
			x.zIndex = 0;
		},500);
	}

	s.toggleActiveProgram = function(x){
		////console.log(x);
		if(x.pop.state == true){
			x.pop.hide();
			setDownZindex(x);
		} else{
			s.setActiveProgram(x);
		}
	}



	s.log = console.log;


}]);
